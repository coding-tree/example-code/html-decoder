let input = document.getElementById("input");
let output = document.getElementById("output");
let button = document.getElementById("button");

button.addEventListener("click", () => {
  let encodedText = input.value;
  let decodedText = encodedText.replace("%2F", "/");
  output.innerText = decodedText;
});
